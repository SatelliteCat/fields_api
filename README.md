Build server:

`go build -v ./cmd/apiserver`

Migration create:

`.\migrate.windows-amd64.exe create -ext sql -dir migrations create_users`

Migration:

`.\migrate.windows-amd64.exe -path migrations -database "postgres://localhost:5433/fields_api?sslmode=disable&user=postgres&password=1234" up`