module bitbucket.org/SatelliteCat/fields_api

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-ozzo/ozzo-validation/v4 v4.1.0
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.4.0
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200423211502-4bdfaf469ed5
)
